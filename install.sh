#/bin/bash
echo -e "开始安装依赖包\n"
pip install -r requirements.txt
echo -e "添加环境变量\n"
echo "export C_FORCE_ROOT="true"" >>/etc/profile
source /etc/profile

echo -e "初始化数据库\n"
python manage.py makemigrations
python manage.py migrate

echo -e "建立管理员账号：\n"
python manage.py syncdb

echo -e "安装完成\n"
echo "请执行：nohup python manage.py celery worker -c 5 --loglevel=info >> logs/celery.log &  启动任务"
