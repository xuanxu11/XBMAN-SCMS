#!/usr/bin/env python
#-*- coding:utf-8 -*-

# Create your views here.
import django
from django.shortcuts import render_to_response,render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import auth
from django.template.context import RequestContext
from django.contrib.auth.decorators import login_required
import json
import logging
import code
import froms




def login(request):
    if request.method == "POST":
        username = request.POST.get('email')
        password = request.POST.get('password')
        user = auth.authenticate(username=username,password=password)
        if user is not None:
            if user.valid_end_time: #设置了end time
                if django.utils.timezone.now() > user.valid_begin_time and django.utils.timezone.now()  < user.valid_end_time:
                    auth.login(request,user)
                    request.session.set_expiry(60*30)
                    #print 'session expires at :',request.session.get_expiry_date()
                    return HttpResponseRedirect('/index/')
                else:
                    return render(request,'login.html',{'login_err': 'User account is expired,please contact your IT guy for this!'})
            elif django.utils.timezone.now() > user.valid_begin_time:
                    auth.login(request,user)
                    request.session.set_expiry(60*30)
                    return HttpResponseRedirect('/index/')

        else:
            return render(request,'login.html',{'login_err': '您输入的用户名或密码错误,请重新输入！'})
    else:
        return render(request, 'login.html')

@login_required
def logout(request):
    auth.logout(request)
    return HttpResponseRedirect("/login/")


@login_required
def checkpasswork(request):
    if request.method=='POST':
        try:
            username=request.POST.get('username')
            oidpassword=request.POST.get('oldpassword')
            password=request.POST.get('password')
            user= auth.authenticate(username=username,password=oidpassword)
            user.set_password(password)
            user.save()
            if user and user.is_active:
                auth.login(request, user)
                return HttpResponseRedirect("/index/")
            log = logging.getLogger('XieYin.app')
            log.error('重新下发用户认证证书失败！')
            return HttpResponseRedirect("/login/")
        except AttributeError,e:
            return HttpResponseRedirect("/login/")

@login_required
def index(request):
    # log = logging.getLogger('XieYin.app')
    # log.info('dfsdfdsf')
    containerd,mainnn,business = code.index(request)
    return render(request, 'index.html',{"containerd": containerd,"mainnn": mainnn,"business": business},
                      context_instance=RequestContext(request))


@login_required
def help(request):
    return render(request, 'help.html',context_instance=RequestContext(request))

@login_required
def pcmanager(request):
    if request.method == "POST":
        if code.pcmanage_post(request):
            return HttpResponseRedirect("/pcmanager/")
        else:
            contacts, group_list = code.pcmamage_get(request)
            return render(request,'PcManagement.html',{"topics": contacts,'group_list':group_list,'zhuangtai':True},
                          context_instance=RequestContext(request))
    elif request.method == 'GET':
        contacts,group_list = code.pcmamage_get(request)
        return render(request,'PcManagement.html', {"topics": contacts,'group_list':group_list},
                      context_instance=RequestContext(request))

@login_required
def deldevice(request):
    if code.del_device(request):
        return HttpResponse(json.dumps('true'))

@login_required
def groupmodify(request):
    if request.method == "POST":
        if code.group_post(request):
            return HttpResponseRedirect("/groupmodify/")
        else:
            contacts = code.group_get(request)
            return render(request, 'group-conf.html',
                          {"topics": contacts,  'zhuangtai': True},
                          context_instance=RequestContext(request))
    elif request.method == 'GET':
        contacts = code.group_get(request)
        return render(request, 'group-conf.html', {"topics": contacts},
                      context_instance=RequestContext(request))

@login_required
def delgroup(request):
    if code.del_group(request):
        return HttpResponse(json.dumps('true'))

@login_required
def editgroup(request):
    if code.edit_group(request):
        return HttpResponseRedirect("/groupmodify/")

@login_required
def confile(request):
    if request.method == "POST":
        if code.config_post(request):
            return HttpResponseRedirect("/confile/")
        else:
            contacts,profile_list,nginx_list,tomcat_list = code.config_get(request)
            return render(request, 'confile.html',
                          {"contacts": contacts,"profile_list":profile_list,"nginx_list":nginx_list,"tomcat_list":tomcat_list,'zhuangtai': True},
                          context_instance=RequestContext(request))
    elif request.method == 'GET':
        contacts,profile_list,nginx_list,tomcat_list = code.config_get(request)
        return render(request, 'confile.html', {"contacts": contacts,"profile_list":profile_list,"nginx_list":nginx_list,"tomcat_list":tomcat_list},
                      context_instance=RequestContext(request))
    return render(request, 'confile.html')

@login_required
def editconf(request):
    if request.method == "POST":
        if code.config_edit(request):
            return HttpResponseRedirect("/confile/")

@login_required
def delconf(request):
    if code.del_config(request):
        return HttpResponse(json.dumps('true'))



@login_required
def page(request):
    if request.method == "POST":
            if code.pageadd(request):
                return HttpResponseRedirect("/page/")
            else:
                pass
    elif request.method == 'GET':
        contacts = code.pageget(request)
        return render_to_response('pageapp.html', {'uf': froms.headImg(),'contacts':contacts},
                                  context_instance=RequestContext(request))

def delpage(request):
    if code.del_page(request):
        return HttpResponse(json.dumps('true'))

@login_required
def nginxpush(request):
    if request.method == "POST":
        if code.nginxpush_post(request):
            contacts, config, group = code.nginxpush_get(request)
            return render_to_response('flot.html', {'contacts': contacts, 'config': config, 'group': group,'zhuangtai':True},
                                      context_instance=RequestContext(request))
    elif request.method == 'GET':
        contacts,config,group = code.nginxpush_get(request)
        return render_to_response('flot.html', {'contacts':contacts,'config':config,'group':group,'zhuangtai':False},
                                  context_instance=RequestContext(request))

@login_required
def tomcatpush(request):
    if request.method == "POST":
        if code.tomcatpush_post(request):
            contacts, config, group = code.tomcatpush_get(request)
            return render_to_response('morros.html',
                                      {'contacts': contacts, 'config': config, 'group': group, 'zhuangtai': True},
                                      context_instance=RequestContext(request))
    elif request.method == 'GET':
        contacts, config, group = code.tomcatpush_get(request)
        return render_to_response('morros.html',{'contacts':contacts,'config':config,'group':group},
                                  context_instance=RequestContext(request))

@login_required
def nginxinstall(request):
    if request.method == "POST":
        if code.ninstall_post(request):
            contacts, config, group = code.ninstall_get(request)
            return render_to_response('nginxinstall.html',
                                      {'contacts': contacts, 'config': config, 'group': group, 'zhuangtai': True},
                                      context_instance=RequestContext(request))
    elif request.method == 'GET':
        contacts, config, group = code.ninstall_get(request)
        return render_to_response('nginxinstall.html', {'contacts':contacts, 'config':config, 'group':group},
                                  context_instance=RequestContext(request))

@login_required
def tomcatinstall(request):
    if request.method == "POST":
        if code.tinstall_post(request):
            contacts, config, group = code.tinstall_get(request)
            return render_to_response('tomcatinstall.html',
                                      {'contacts': contacts, 'config': config, 'group': group, 'zhuangtai': True},
                                      context_instance=RequestContext(request))
    elif request.method == 'GET':
        contacts, config, group = code.tinstall_get(request)
        return render_to_response('tomcatinstall.html', {'contacts':contacts, 'config':config, 'group':group},
                                  context_instance=RequestContext(request))

@login_required
def generate(request):
    return HttpResponse(code.code_generate(request))

@login_required
def tasks_tables(request):
    contacts = code.tasks_get(request)
    return render_to_response('tasks.html',{"topics": contacts},
                              context_instance=RequestContext(request))

@login_required
def cmdrun(request):
    if request.method == "POST":
        data = code.cmdrun(request)
        return HttpResponse(json.dumps(data))
    elif request.method == 'GET':
        #使用tomcatinstall的方法获取机器和组
        contacts, config, group = code.tinstall_get(request)
        return render_to_response('forms.html',{'contacts':contacts, 'group':group},
                                  context_instance=RequestContext(request))
#文件推送功能
# @login_required
# def filepush(request):
#     if request.method == 'GET':
#         contacts, config, group = code.tinstall_get(request)
#         return render_to_response('filepush.html', {'contacts': contacts, 'group': group},
#                                   context_instance=RequestContext(request))